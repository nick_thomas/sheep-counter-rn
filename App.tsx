import { StatusBar } from "expo-status-bar";
import { useState } from "react";
import { StyleSheet, Text, TextInput, View, Image } from "react-native";
import SheepContainer from "./components/SheepContainer";
import SheepDisplay from "./components/SheepViewer";

export default function App() {
  const [value, onChangeText] = useState<string>("");
  return (
    <View style={styles.container}>
      <Text>Sheep Counter</Text>
      <SheepDisplay />
      <StatusBar style="auto" />
      <View>
        {value !== "" && (
          <Text style={styles.counterDisplay}>
            The number of sheeps counter so far is {value}
          </Text>
        )}
      </View>
      <SheepContainer value={value} onChangeText={onChangeText} />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
  counter: {
    backgroundColor: "#fee",
    borderBottomColor: "#000000",
    borderBottomWidth: 1,
  },
  inputStyles: {
    padding: 10,
  },
  sheepImage: {
    width: 150,
    height: 150,
  },
  counterDisplay: {
    color: "000",
  },
});
