import { StyleSheet, TextInput, View } from "react-native";

interface PropsI {
  value: string;
  onChangeText: Function;
}

const SheepContainer = ({ value, onChangeText }: PropsI) => {
  return (
    <View style={styles.counter}>
      <TextInput
        style={styles.inputStyles}
        keyboardAppearance={"light"}
        keyboardType={"numeric"}
        value={value.toString()}
        onChangeText={(values) => {
          if (values.length !== 0) {
            return onChangeText(values);
          } else {
            return onChangeText("");
          }
        }}
        editable
        maxLength={40}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  counter: {
    backgroundColor: "#fee",
    borderBottomColor: "#000000",
    borderBottomWidth: 1,
  },
  inputStyles: {
    padding: 10,
  },
});
export default SheepContainer;
