import { StyleSheet, Text, TextInput, View, Image } from "react-native";
const SheepDisplay = () => {
  return (
    <View>
      <Image
        style={styles.sheepImage}
        source={require("./assets/resources/cute-sitting-black-sheep-made-plasticine-isolated-white-clay-black-sheep-different-rebel_597321-196.jpg")}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  sheepImage: {
    width: 150,
    height: 150,
  },
});
export default SheepDisplay;
